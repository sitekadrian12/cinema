package com.as.prz.cinema.repository;

import com.as.prz.cinema.model.Seance;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.List;

@Repository
public interface SeanceRepository extends CrudRepository<Seance, Long> {

    @Query(nativeQuery = true, value =
            "Select *" +
                    "FROM cinema.seance s " +
                    "where s.room_id = ?3" +
                    "  AND ((" +
                    "               s.date_time_start Between ?1 AND ?2 OR" +
                    "               s.date_time_end Between ?1 AND ?2) OR (" +
                    "               ?1 Between s.date_time_start AND s.date_time_end" +
                    "               OR ?2" +
                    "                   Between s.date_time_start AND s.date_time_end))")
    List<Seance> findByDates(LocalDateTime dateTimeStart, LocalDateTime dateTimeEnd, Long roomId);

    List findAll();

}
