package com.as.prz.cinema.repository;

import com.as.prz.cinema.model.Reservation;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Repository
public interface ReservationRepository extends CrudRepository<Reservation, Long> {

    @Query(value = "select * from cinema.reservation r JOIN cinema.reservation_details rd ON r.id = rd.reservation_id where r.seance_id = ?1 AND rd.seat_number in(?2) AND r.reservation_status not like ?3", nativeQuery = true)
    List<Reservation> findAllBySeanceAndReservation(Long seanceId, Set<Short> seats, String s);

    List findAll();
}
