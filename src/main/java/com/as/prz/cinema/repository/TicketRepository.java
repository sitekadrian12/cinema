package com.as.prz.cinema.repository;

import com.as.prz.cinema.model.Reservation;
import com.as.prz.cinema.model.Ticket;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface TicketRepository extends CrudRepository<Ticket, Long> {


    List<Ticket> findAllBySeance_IdAndSeatNumberIn(Long seanceId, Set<Short> seats);

    List<Ticket> findAllByReservation(Reservation reservation);

    List<Ticket> findAllBySeance_IdAndSeatNumber(Long seanceId, Short seats);

    List findAll();

}
