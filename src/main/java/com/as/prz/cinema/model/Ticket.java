package com.as.prz.cinema.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.With;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Getter
@NoArgsConstructor
@AllArgsConstructor
@SequenceGenerator(schema = "mapping", name = "ticket_seq", sequenceName = "ticket_id_seq", allocationSize = 1)
@Table(schema = "cinema")
public class Ticket {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "seance_id", foreignKey = @ForeignKey(name = "ticket_seance_fk"), nullable = false)
    private Seance seance;

    @Column(precision = 8, scale = 2)
    private BigDecimal priceOfMovie;

    @Column(precision = 8, scale = 2)
    private BigDecimal priceOfGlasses;

    @ManyToOne
    @JoinColumn(name = "reservation_id", foreignKey = @ForeignKey(name = "ticket_reservation_fk"))
    private Reservation reservation;

    private Short seatNumber;

}
