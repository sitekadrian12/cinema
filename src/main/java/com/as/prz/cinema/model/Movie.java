package com.as.prz.cinema.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.With;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;

@Entity
@Getter
@NoArgsConstructor
@AllArgsConstructor
@SequenceGenerator(schema = "mapping", name = "movie_seq", sequenceName = "movie_id_seq", allocationSize = 1)
@Table(schema = "cinema")
public class Movie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;

    private Long length;

    private String type;

    @Enumerated(EnumType.STRING)
    private Status status;

    @Column(name = "release_date")
    private LocalDateTime releaseDate;

    public enum Status {
        AVAILABLE,
        UNAVAILABLE,
        PREMIERE,
        PREVIEW

    }
}
