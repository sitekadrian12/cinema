package com.as.prz.cinema.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor
@SequenceGenerator(schema = "mapping", name = "room_seq", sequenceName = "room_id_seq", allocationSize = 1)
@Table(schema = "cinema")
public class Room {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, name = "number_of_seats")
    private Short numberOfSeats;

    private String name;

}
