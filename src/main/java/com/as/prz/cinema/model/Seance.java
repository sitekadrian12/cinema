package com.as.prz.cinema.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.With;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;

@Entity
@Getter
@NoArgsConstructor
@AllArgsConstructor
@SequenceGenerator(schema = "mapping", name = "seance_seq", sequenceName = "seance_id_seq", allocationSize = 1)
@Table(schema = "cinema")
public class Seance {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "movie_id", foreignKey = @ForeignKey(name = "seance_movie_fk"), nullable = false)
    private Movie movie;

    @ManyToOne
    @JoinColumn(name = "room_id", foreignKey = @ForeignKey(name = "seance_room_fk"), nullable = false)
    private Room room;

    @Column(nullable = false)
    private Boolean is3d;

    private LocalDateTime dateTimeStart;

    private LocalDateTime dateTimeEnd;


}
