package com.as.prz.cinema.model;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SequenceGenerator(schema = "mapping", name = "reservation_seq", sequenceName = "reservation_id_seq", allocationSize = 1)
@Table(schema = "cinema")
public class Reservation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private ReservationStatus reservationStatus;

    @ManyToOne
    @JoinColumn(name = "seance_id", foreignKey = @ForeignKey(name = "reservation_seance_fk"), nullable = false)
    private Seance seance;

    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            mappedBy = "reservation"
    )
    Set<ReservationDetails> reservationDetailsList;

    public enum ReservationStatus {
        Pending,
        Canceled,
        PAID
    }

}
