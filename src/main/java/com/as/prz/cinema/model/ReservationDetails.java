package com.as.prz.cinema.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.With;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor
@AllArgsConstructor
@SequenceGenerator(schema = "mapping", name = "reservation_detail_seq", sequenceName = "reservation_detail_id_seq", allocationSize = 1)
@Table(schema = "cinema")
public class ReservationDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Short seatNumber;

    @ManyToOne
    @JoinColumn(name = "reservation_id", foreignKey = @ForeignKey(name = "reservation_detail_reservation_fk"), nullable = false)
    private Reservation reservation;


}
