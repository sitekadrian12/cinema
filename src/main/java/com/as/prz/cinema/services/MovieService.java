package com.as.prz.cinema.services;

import com.as.prz.cinema.dto.MovieDTO;
import com.as.prz.cinema.model.Movie;
import com.as.prz.cinema.model.Reservation;
import com.as.prz.cinema.repository.MovieRepository;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import java.util.List;
import java.util.Optional;

@Service
public class MovieService {

    private final MovieRepository movieRepository;

    public MovieService(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    public void create(MovieDTO dto) {
        Movie movie = new Movie(null, dto.getTitle(), dto.getLength(), dto.getType(), dto.getStatus(), dto.getReleaseDate());
        movieRepository.save(movie);
    }

    public void update(Long id, MovieDTO dto) throws NotFoundException {
        getMovie(id);

        Movie movie = new Movie(id, dto.getTitle(), dto.getLength(), dto.getType(), dto.getStatus(), dto.getReleaseDate());
        movieRepository.save(movie);
    }

    public void delete(Long id) throws NotFoundException {
        Movie movie = getMovie(id);
        movieRepository.save(movie);
    }

    public List<Movie> getAll () {
        return movieRepository.findAll();
    }

    public Movie getMovie(Long id) throws NotFoundException {
        Optional<Movie> movie = movieRepository.findById(id);
        if (movie.isPresent())
            return movie.get();
        throw new NotFoundException("Movie not found");
    }

}
