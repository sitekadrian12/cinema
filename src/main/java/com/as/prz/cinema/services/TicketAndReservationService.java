package com.as.prz.cinema.services;

import com.as.prz.cinema.dto.ReservationDTO;
import com.as.prz.cinema.dto.TicketDTO;
import com.as.prz.cinema.model.*;
import com.as.prz.cinema.repository.ReservationRepository;
import com.as.prz.cinema.repository.TicketRepository;
import javassist.NotFoundException;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class TicketAndReservationService {

    private final ReservationRepository reservationRepository;
    private final SeanceService seanceService;
    private final TicketRepository ticketRepository;

    public TicketAndReservationService(ReservationRepository reservationRepository, SeanceService seanceService, TicketRepository ticketRepository) {
        this.reservationRepository = reservationRepository;
        this.seanceService = seanceService;
        this.ticketRepository = ticketRepository;
    }

    public void cancelReservation(Long id) throws NotFoundException {
        Reservation reservation = getReservation(id);
        reservation.setReservationStatus(Reservation.ReservationStatus.Canceled);
        reservationRepository.save(reservation);
    }

    public void createReservation(ReservationDTO dto) throws Exception {

        Seance seance = seanceService.getSeance(dto.getSeanceId());

        Short max = (short) dto.getSeats().stream().mapToInt(r -> r).max().orElseThrow(() -> new Exception("no seats"));
        if (max > seance.getRoom().getNumberOfSeats())
            throw new Exception("there is no such place as ".concat(max.toString()));

        canBuyPlace(dto.getSeanceId(), dto.getSeats());

        Reservation reservation = new Reservation(dto.getSeanceId(), Reservation.ReservationStatus.Pending, seance, new HashSet<>());
        Set<ReservationDetails> reservationDetails = dto.getSeats().stream().map(row -> new ReservationDetails(null, row, reservation)).collect(Collectors.toSet());
        reservation.setReservationDetailsList(reservationDetails);
        reservationRepository.save(reservation);

    }

    public List<Reservation> getAllReservation() {
        return reservationRepository.findAll();
    }

    public void createTicket(TicketDTO ticketDTO) throws Exception {
        Set<Short> seats = new HashSet<>();
        seats.add(ticketDTO.getSeatNumber());
        canBuyPlace(ticketDTO.getSeanceId(), seats);
        Seance seance = seanceService.getSeance(ticketDTO.getSeanceId());
        if (seance.getRoom().getNumberOfSeats() < ticketDTO.getSeatNumber())
            throw new Exception("there is no such place as ".concat(ticketDTO.getSeatNumber().toString()));

        Ticket ticket = new Ticket(null, seance, BigDecimal.valueOf(14), seance.getIs3d() ? BigDecimal.TEN : BigDecimal.ZERO, null, ticketDTO.getSeatNumber());
        ticketRepository.save(ticket);
    }

    public void acceptReservation(Long id) throws Exception {
        Reservation reservation = getReservation(id);
        if (reservation.getReservationStatus() != Reservation.ReservationStatus.Pending)
            throw new Exception("Reservation is not available");
        List<Ticket> tickets = reservation.getReservationDetailsList().stream().map(row -> new Ticket(null, reservation.getSeance(), BigDecimal.valueOf(14), reservation.getSeance().getIs3d() ? BigDecimal.TEN : BigDecimal.ZERO, reservation, row.getSeatNumber())).collect(Collectors.toList());
        ticketRepository.saveAll(tickets);
        buyoutReservation(reservation);
    }

    public List<Ticket> getAllTickets() {
        return ticketRepository.findAll();
    }

    private void buyoutReservation(Reservation reservation) throws NotFoundException {
        reservation.setReservationStatus(Reservation.ReservationStatus.PAID);
        reservationRepository.save(reservation);
    }


    public Reservation getReservation(Long id) throws NotFoundException {
        Optional<Reservation> reservation = reservationRepository.findById(id);
        if (reservation.isPresent())
            return reservation.get();
        throw new NotFoundException("Movie not found");
    }


    private void canBuyPlace(Long seanceId, Set<Short> seats) throws Exception {
        List<Reservation> rlist = reservationRepository.findAllBySeanceAndReservation(seanceId, seats, Reservation.ReservationStatus.Canceled.toString());
        List<Ticket> tickets = ticketRepository.findAllBySeance_IdAndSeatNumberIn(seanceId, seats);
        if(rlist.isEmpty() && tickets.isEmpty())
            return;
        throw new Exception("seats have been taken");
    }

}
