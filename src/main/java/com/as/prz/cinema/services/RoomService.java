package com.as.prz.cinema.services;

import com.as.prz.cinema.dto.MovieDTO;
import com.as.prz.cinema.dto.RoomDTO;
import com.as.prz.cinema.model.Movie;
import com.as.prz.cinema.model.Room;
import com.as.prz.cinema.repository.MovieRepository;
import com.as.prz.cinema.repository.RoomRepository;
import javassist.NotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RoomService {

    private final RoomRepository roomRepository;

    public RoomService(MovieRepository movieRepository, RoomRepository roomRepository) {
        this.roomRepository = roomRepository;
    }

    public void create(RoomDTO dto) {
        Room room = new Room(null, dto.getNumberOfSeats(), dto.getName());
        roomRepository.save(room);
    }

    public void update(Long id, RoomDTO dto) throws NotFoundException {
        getRoom(id);

        Room room = new Room(id, dto.getNumberOfSeats(), dto.getName());
        roomRepository.save(room);
    }

    public void delete(Long id) throws NotFoundException {
        Room room = getRoom(id);
        roomRepository.save(room);
    }


    public Room getRoom(Long id) throws NotFoundException {
        Optional<Room> room = roomRepository.findById(id);
        if (room.isPresent())
            return room.get();
        throw new NotFoundException("Room not found");
    }

    public List<Room> getAll() {
        return roomRepository.findAll();
    }

}
