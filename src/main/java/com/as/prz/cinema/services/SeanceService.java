package com.as.prz.cinema.services;

import com.as.prz.cinema.dto.MovieDTO;
import com.as.prz.cinema.dto.RoomDTO;
import com.as.prz.cinema.dto.SeanceDTO;
import com.as.prz.cinema.model.Movie;
import com.as.prz.cinema.model.Room;
import com.as.prz.cinema.model.Seance;
import com.as.prz.cinema.repository.SeanceRepository;
import javassist.NotFoundException;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Optional;

@Service
public class SeanceService {

    private final MovieService movieService;
    private final RoomService roomService;
    private final SeanceRepository seanceRepository;


    public SeanceService(MovieService movieService, RoomService roomService, SeanceRepository seanceRepository) {
        this.movieService = movieService;
        this.roomService = roomService;
        this.seanceRepository = seanceRepository;
    }

    public void delete(Long id) throws NotFoundException {
        Seance seance = getSeance(id);
        seanceRepository.save(seance);
    }

    public List<Seance> getAll() {
        return seanceRepository.findAll();
    }

    public Seance getSeance(Long id) throws NotFoundException {
        Optional<Seance> seance = seanceRepository.findById(id);
        if (seance.isPresent())
            return seance.get();
        throw new NotFoundException("Movie not found");
    }

    public void create(SeanceDTO dto) throws Exception {

        List<Seance> seances = seanceRepository.findByDates(dto.getDateTimeStart(), dto.getDateTimeEnd(), dto.getRoomId());
        if (!seances.isEmpty())
            throw new Exception("seances colliding with each oder");
        Movie movie = movieService.getMovie(dto.getMovieId());
        checkTime(dto, movie);
        Room room = roomService.getRoom(dto.getMovieId());
        Seance seance = new Seance(null, movie, room, dto.getIs3d(), dto.getDateTimeStart(), dto.getDateTimeEnd());
        seanceRepository.save(seance);
    }

    private void checkTime(SeanceDTO dto, Movie movie) throws Exception {
        if (dto.getDateTimeEnd().toEpochSecond(ZoneOffset.UTC) - dto.getDateTimeStart().toEpochSecond(ZoneOffset.UTC) - (movie.getLength() * 60) < 0)
            throw new Exception("to small amount of time");
    }

    public void update(Long id, SeanceDTO dto) throws Exception {
        List<Seance> seances = seanceRepository.findByDates(dto.getDateTimeStart(), dto.getDateTimeEnd(), dto.getRoomId());
        if (!seances.isEmpty()) {
            if (!seances.get(0).getId().equals(id))
                throw new Exception("seances colliding with eachoder");
        }
        Movie movie = movieService.getMovie(dto.getMovieId());
        Room room = roomService.getRoom(dto.getMovieId());
        Seance seance = new Seance(null, movie, room, dto.getIs3d(), dto.getDateTimeStart(), dto.getDateTimeEnd());
        seanceRepository.save(seance);
    }

}