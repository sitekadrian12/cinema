package com.as.prz.cinema.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class SeanceFullDTO {

    private Long id;

    private String movieName;

    private String roomName;

    private Boolean is3d;

    private String dateTimeStart;

    private String dateTimeEnd;


}
