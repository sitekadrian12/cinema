package com.as.prz.cinema.dto;

import com.as.prz.cinema.model.ReservationDetails;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.With;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ReservationDTO {

    private Long seanceId;

    private Set<Short> seats;

}
