package com.as.prz.cinema.dto;

import com.as.prz.cinema.model.Movie;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class MovieFullDTO {

    private Long id;

    private String releaseDate;

    private String title;

    private Long length;

    private String type;

    @Enumerated(EnumType.STRING)
    private Movie.Status status;

}
