package com.as.prz.cinema.dto;

import com.as.prz.cinema.model.Movie;
import com.as.prz.cinema.model.Room;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class SeanceDTO {

    private Long movieId;

    private Long roomId;

    private Boolean is3d;

    private LocalDateTime dateTimeStart;

    private LocalDateTime dateTimeEnd;


}
