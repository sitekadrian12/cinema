package com.as.prz.cinema.dto;

import com.as.prz.cinema.model.Reservation;
import com.as.prz.cinema.model.Seance;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TicketDTO {

    private Long seanceId;

    private Short seatNumber;

}
