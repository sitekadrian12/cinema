package com.as.prz.cinema.dto;

import com.as.prz.cinema.model.Reservation;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.List;
import java.util.Set;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ReservationFullDTO {

    private Long id;

    private String movie;

    private List<Short> seats;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private Reservation.ReservationStatus reservationStatus;

}
