package com.as.prz.cinema.dto;

import com.as.prz.cinema.model.Movie;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.With;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class MovieDTO {

    private String title;

    private Long length;

    private String type;

    @Enumerated(EnumType.STRING)
    private Movie.Status status;

    private LocalDateTime releaseDate;

}
