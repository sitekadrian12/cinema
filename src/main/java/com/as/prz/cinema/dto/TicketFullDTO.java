package com.as.prz.cinema.dto;

import com.as.prz.cinema.model.Reservation;
import com.as.prz.cinema.model.Seance;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class TicketFullDTO {

    private Long id;

    private String movie;

    private String dateStart;

    @Column(precision = 8, scale = 2)
    private BigDecimal priceOfMovie;

    @Column(precision = 8, scale = 2)
    private BigDecimal priceOfGlasses;

    private Long reservation_id;

    private Short seatNumber;

}
