package com.as.prz.cinema.controller;

import com.as.prz.cinema.dto.*;
import com.as.prz.cinema.model.Reservation;
import com.as.prz.cinema.model.Room;
import com.as.prz.cinema.model.Ticket;
import com.as.prz.cinema.services.TicketAndReservationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.stream.Collectors;

@RestController
@Api(tags = "Ticket and reservation api", produces = "application/json", consumes = "application/json")
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
class TicektAndReservationController {

    private final TicketAndReservationService ticketAndReservationService;

    @Autowired
    public TicektAndReservationController(TicketAndReservationService ticketAndReservationService) {
        this.ticketAndReservationService = ticketAndReservationService;
    }

    @PostMapping(value = "/ticket")
    @ApiOperation(value = "Buy ticket", notes = "Buy ticket", response = Void.class)
    public ResponseEntity buyTicket(
            @RequestBody @Valid @NotNull TicketDTO ticketDTO
    ) {
        try {
            ticketAndReservationService.createTicket(ticketDTO);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok().build();
    }

    @GetMapping(value = "/ticket")
    @ApiOperation(value = "Get all tickets", notes = "get all tickets", response = TicketFullDTO.class)
    public ResponseEntity getTickets() {
        try {
            ticketAndReservationService.getAllTickets().stream().map(r -> new TicketFullDTO(r.getId(), r.getSeance().getMovie().getTitle(),r.getSeance().getDateTimeStart().toString(), r.getPriceOfMovie(), r.getPriceOfGlasses(), r.getReservation().getId(), r.getSeatNumber()));
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok().build();
    }

    @PutMapping(value = "/reservation")
    @ApiOperation(value = "Create reservation ", notes = "Create reservation", response = Void.class)
    public ResponseEntity createReservation(
            @RequestBody @Valid @NotNull ReservationDTO reservationDTO) {

        try {
            ticketAndReservationService.createReservation(reservationDTO);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok().build();
    }

    @GetMapping(value = "/reservation")
    @ApiOperation(value = "Get all reservations", notes = "Get all reservations", response = ReservationFullDTO.class)
    public ResponseEntity getAll() {

        return ResponseEntity.ok(ticketAndReservationService.getAllReservation().stream().map(r -> new ReservationFullDTO(r.getId(), r.getSeance().getMovie().getTitle(), r.getReservationDetailsList().stream().map(row -> row.getSeatNumber()).collect(Collectors.toList()), r.getReservationStatus())));
    }


    @PutMapping(value = "/reservation/{id}")
    @ApiOperation(value = "Accept reservation", notes = "Accept reservation", response = Void.class)
    public ResponseEntity accept(
            @PathVariable Long id) {

        try {
            ticketAndReservationService.acceptReservation(id);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok().build();
    }

    @PutMapping(value = "/reservation/{id}/cancel")
    @ApiOperation(value = "Cancel reservation", notes = "Cancel reservation", response = Void.class)
    public ResponseEntity delete(
            @PathVariable Long id) {

        try {
            ticketAndReservationService.acceptReservation(id);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok().build();
    }
}
