package com.as.prz.cinema.controller;

import com.as.prz.cinema.dto.SeanceDTO;
import com.as.prz.cinema.dto.SeanceFullDTO;
import com.as.prz.cinema.model.Seance;
import com.as.prz.cinema.services.SeanceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@Api(tags = "Seance api", produces = "application/json", consumes = "application/json")
@RequestMapping( produces = MediaType.APPLICATION_JSON_VALUE)
class SeanceController {

    private final SeanceService seanceService;

    @Autowired
    public SeanceController(SeanceService seanceService) {
        this.seanceService = seanceService;
    }

    @PostMapping(value = "/seance")
    @ApiOperation(value = "Create seance", notes = "Create seance", response = Void.class)
    public ResponseEntity createSeance(
            @RequestBody @Valid @NotNull SeanceDTO seanceDTO
    ) {

        try {
            seanceService.create(seanceDTO);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok().build();
    }

    @PutMapping(value = "/seance/{id}")
    @ApiOperation(value = "update seance", notes = "Update seance", response = Void.class)
    public ResponseEntity updateSeance(
            @RequestBody @Valid @NotNull SeanceDTO seanceDTO,
            @PathVariable Long id) {

        try {
            seanceService.update(id, seanceDTO);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok().build();
    }

    @GetMapping(value = "/seance")
    @ApiOperation(value = "Get all seances", notes = "Get all", response = SeanceFullDTO.class)
    public ResponseEntity getAll() {

        return ResponseEntity.ok(seanceService.getAll().stream().map(r -> new SeanceFullDTO(r.getId(),r.getMovie().getTitle(),r.getRoom().getName(),r
        .getIs3d(),r.getDateTimeStart().toString(),r.getDateTimeEnd().toString())));
    }


    @DeleteMapping(value = "/seance/{id}")
    @ApiOperation(value = "Delete seance", notes = "Get all by dates", response = Void.class)
    public ResponseEntity delete(
            @PathVariable Long id) {

        try {
            seanceService.delete(id);
        } catch (NotFoundException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok().build();
    }
}
