package com.as.prz.cinema.controller;

import com.as.prz.cinema.dto.MovieDTO;
import com.as.prz.cinema.dto.MovieFullDTO;
import com.as.prz.cinema.model.Movie;
import com.as.prz.cinema.services.MovieService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@Api(tags = "Movie api", produces = "application/json", consumes = "application/json")
@RequestMapping( produces = MediaType.APPLICATION_JSON_VALUE)
class MovieController {

    private final MovieService movieService;

    @Autowired
    public MovieController(MovieService movieService) {
        this.movieService = movieService;
    }

    @PostMapping(value = "/movie")
    @ApiOperation(value = "Get client accounting", notes = "Create movie", response = Void.class)
    public ResponseEntity createMovie(
            @RequestBody @Valid @NotNull MovieDTO movieDTO
    ) {

        movieService.create(movieDTO);
        return ResponseEntity.ok().build();
    }

    @PutMapping(value = "/movie/{id}")
    @ApiOperation(value = "Get client accounting", notes = "Update movie", response = Void.class)
    public ResponseEntity updateMovie(
            @RequestBody @Valid @NotNull MovieDTO movieDTO,
            @PathVariable Long id) {

        try {
            movieService.update(id, movieDTO);
        } catch (NotFoundException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok().build();
    }

    @GetMapping(value = "/movie")
    @ApiOperation(value = "Get client accounting", notes = "Get all", response = Movie.class)
    public ResponseEntity getAll() {

        return ResponseEntity.ok(movieService.getAll().stream().map(r -> new MovieFullDTO(r.getId(),r.getReleaseDate().toString(),r.getTitle(),r.getLength(),r.getType(),r.getStatus())));
    }


    @DeleteMapping(value = "/movie/{id}")
    @ApiOperation(value = "Get client accounting", notes = "Get all by dates", response = Void.class)
    public ResponseEntity delete(
            @PathVariable Long id) {

        try {
            movieService.delete(id);
        } catch (NotFoundException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok().build();
    }
}
