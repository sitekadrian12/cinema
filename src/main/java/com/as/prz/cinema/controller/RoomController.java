package com.as.prz.cinema.controller;

import com.as.prz.cinema.dto.RoomDTO;
import com.as.prz.cinema.model.Room;
import com.as.prz.cinema.services.RoomService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@Api(tags = "Room api", produces = "application/json", consumes = "application/json")
@RequestMapping( produces = MediaType.APPLICATION_JSON_VALUE)
class RoomController {

    private final RoomService roomService;

    @Autowired
    public RoomController(RoomService roomService) {
        this.roomService = roomService;
    }

    @PostMapping(value = "/room")
    @ApiOperation(value = "Create room", notes = "Create room", response = Void.class)
    public ResponseEntity createRoom(
            @RequestBody @Valid @NotNull RoomDTO roomDTO
    ) {

        roomService.create(roomDTO);
        return ResponseEntity.ok().build();
    }

    @PutMapping(value = "/room/{id}")
    @ApiOperation(value = "update room", notes = "Update room", response = Void.class)
    public ResponseEntity updateRoom(
            @RequestBody @Valid @NotNull RoomDTO roomDTO,
            @PathVariable Long id) {

        try {
            roomService.update(id, roomDTO);
        } catch (NotFoundException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok().build();
    }

    @GetMapping(value = "/room")
    @ApiOperation(value = "Get all rooms", notes = "Get all", response = Room.class)
    public ResponseEntity getAll() {

        return ResponseEntity.ok(roomService.getAll());
    }


    @DeleteMapping(value = "/room/{id}")
    @ApiOperation(value = "Delete room", notes = "Get all by dates", response = Void.class)
    public ResponseEntity delete(
            @PathVariable Long id) {

        try {
            roomService.delete(id);
        } catch (NotFoundException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok().build();
    }
}
